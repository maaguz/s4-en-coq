(*Load SyntaxS4LP.*)

(*
Propositional variables
*)
Inductive var: Set := 
p : nat -> var.


(*
Formulas
*)
Inductive formula: Set :=
| Fal 	: formula (*false constant*)
| Atom	: var -> formula (*atomic proposition*)
| And : formula -> formula -> formula (*F/\'F is formula*) 
| Or  : formula -> formula -> formula (*F\/'F is formula*)
| If 	: formula -> formula -> formula (*F->F is formula*)
| Box	: formula -> formula. (*[]F is formula*)

(*
Our negation is P -> False, as is native in Coq
*)
Definition Not (f:formula): formula :=
(If f Fal).

(*
We only are going to use Ver for particular modal examples from Chellas. Our 
notion of Truth comes rather from the elements of CS (axioms of the system)
*)
Definition Ver: formula := 
(Not Fal).

(*
The diamond operator is going to be used for the notion of modal negative
introspection.
*)
Definition Diam (f:formula): formula := 
(Not (Box (Not f))).

Definition Iif (f1 f2: formula): formula := 
(And (If f1 f2) (If f2 f1)).


Notation "p /\' q" := (And p q) (at level 80, right associativity).

Notation "p \/' q" := (Or p q) (at level 85, right associativity).

Notation "¬ p" := (Not p)	(at level 70, right associativity).

Notation "p --> q" := (If p q) (at level 90, right associativity).

Notation "p <--> q" := (Iif p q) (at level 95, right associativity).

Notation "[] F"	:= (Box F)	(at level 65, right associativity).

Notation "<> F"	:= (Diam F) (at level 65, right associativity).



(*
Some examples of formulas
*)
Check ¬ Atom (p 1).
Check Atom (p 1) --> Atom (p 2).
Check [] [] (Atom (p 1) --> Atom (p 2)).
Check (And (Atom (p 1)) (Atom (p 2))).

Check ¬ Atom (p 1).
Check (And (Atom (p 1)) (Atom (p 2))).



(*Load Axiomatization.*)
(**Propositional Logic**)
Definition AxThen1 (P Q:formula) := (*THEN 1*)
(P --> (Q --> P)).
Definition AxThen2 (P Q R:formula) := (*THEN 2*) 
((P --> (Q --> R)) --> ((P --> Q) --> (P --> R))).
Definition AxAnd1 (P Q:formula) := (*AND 1*)
((P /\' Q) --> P).
Definition AxAnd2 (P Q:formula) := (*AND 2*)
((P /\' Q) --> Q).
Definition AxAnd3 (P Q:formula) := (*AND 3*)
(P --> (Q --> (P /\' Q))).
Definition AxOr1 (P Q:formula) := (*OR 1*)
(P --> (P \/' Q)).
Definition AxOr2 (P Q:formula) := (*OR 2*)
(Q --> (P \/' Q)).
Definition AxOr3 (P Q R:formula) := (*OR 3*)
((P --> R) --> ((Q --> R) --> ((P \/' Q) --> R))).
Definition AxFalse (P:formula) := (*FALSE*)
(Fal --> P).

(*Modal Logic S4*)
Definition AxK (P Q: formula) :=
[](P --> Q) --> []P --> []Q.
Definition Ax4 (P: formula) :=
[]P --> [] [] P.
Definition AxT (P: formula) :=
[] P --> P.

(* Modal Logic S5 *)
Definition Ax5 (P: formula) :=
<> P --> [] <> P.


(*Load NaturalDeduction.*)
(*Load SyntaxS4LP.

Load Axiomatization.*)

Parameter Assert: formula->Prop.

Inductive Provable: formula -> Prop :=  
| then1:  forall (P Q:formula),
  Provable(AxThen1 P Q)

| then2:  forall (P Q R: formula),
  Provable(AxThen2 P Q R)

| and1:   forall (P Q: formula),
  Provable(AxAnd1 P Q)

| and2:   forall (P Q: formula),
  Provable(AxAnd2 P Q)

| and3:   forall (P Q:formula),
  Provable(AxAnd3 P Q)

| or1:    forall (P Q: formula),
  Provable(AxOr1 P Q)

| or2:    forall (P Q: formula),
  Provable(AxOr2 P Q)

| or3:    forall (P Q R: formula),
  Provable(AxOr3 P Q R)

| falso:  forall (P: formula),
   Provable(AxFalse P)
(***)

(*S4*)
| modK: forall (P Q: formula),
    Provable (AxK P Q)
| mod4: forall (P:formula),
    Provable (Ax4 P)
| mod5: forall (P:formula),
    Provable (Ax5 P)
| modT: forall (P:formula),
    Provable (AxT P)
(*S4*)
.

Notation " |- phi" := (Provable phi) (at level 30).

Hypotheses Hyp: forall (P:formula),
    Assert P -> Provable P.

(*Modus Ponens IMP-E*)
Hypotheses MP: forall (P Q: formula),
    Provable (P --> Q) ->
    Provable P ->
    Provable Q.

(*Deduction Theorem IMP-I*)
Hypotheses DT: forall (P Q: formula),
    (Assert P -> Provable Q) -> 
    Provable (P --> Q).

(*Necesitation*)
Hypotheses Nec: forall (P:formula),
    Provable P ->
    Provable ([] P).


(*And-I*)
Theorem andI: forall (P Q: formula), 
  Provable P ->
  Provable Q ->
  Provable(P /\' Q).
Proof.
intros.
eapply MP.
eapply MP.
apply and3.
auto.
auto.
Qed.

(*And-E1*)
Theorem andE1: forall (P Q: formula), 
  Provable(P /\' Q) ->
  Provable P.
Proof.
intros.
eapply MP in H.
exact H.
apply and1.
Qed.

(*And-E2*)
Theorem andE2: forall (P Q: formula), 
  Provable(P /\' Q) ->
  Provable Q.
Proof.
intros.
eapply MP in H.
exact H.
apply and2.
Qed.

Theorem orI1: forall (P Q: formula), (*introducci\u00f3n de Or 1*)
  Provable P ->
  Provable (P \/' Q).
Proof.
intros.
eapply MP.
apply or1.
trivial.
Qed.

Theorem orI2: forall (P Q: formula), (*introducci\u00f3n de Or 1*)
  Provable Q ->
  Provable (P \/' Q).
Proof.
intros.
eapply MP.
apply or2.
trivial.
Qed.

Theorem orE: forall (P Q R: formula), (*eliminaci\u00f3n de Or*)
  Provable (P \/' Q) ->
  (Assert P -> Provable R) ->
  (Assert Q -> Provable R) ->
  Provable R.
Proof.
intros.
eapply MP.
eapply MP.
eapply MP.
apply or3.

apply DT.
exact H0.
apply DT.
exact H1.

trivial.
Qed.


Theorem notI: forall P:formula, 
  (Assert P -> Provable Fal) ->
  Provable (¬ P).
Proof.
intros.
unfold Not.
apply DT.
trivial.
Qed.

Theorem notE:  forall P:formula,
  Provable P ->
  Provable (¬ P) ->
  Provable Fal.
Proof.
intros.
unfold Not in H0.
eapply MP.
exact H0.
trivial.
Qed.

Theorem botE: forall P:formula,
  Provable Fal ->
  Provable P.
Proof.
intros.
eapply MP.
apply falso.
trivial.
Qed.

Theorem Kmod: forall (P Q: formula), 
    Provable([](P --> Q)) ->
    Provable([]P) ->
    Provable([]Q).
Proof.
intros.

eapply MP.
eapply MP.
apply modK.

exact H.

trivial.
Qed.

Theorem Tmod: forall (P: formula),
    Provable([] P) -> 
    Provable P.
Proof.
intros.
eapply MP.
apply modT.
auto.
Qed.

Theorem _4mod: forall (P:formula),
    Provable([]P) ->
    Provable([][]P).
Proof.
intros.
eapply MP.
apply mod4.
auto.
Qed.

Theorem _5mod: forall (P:formula),
    Provable(<>P) ->
    Provable([]<>P).
Proof.
intros.
eapply MP.
apply mod5.
auto.
Qed.




(*
A simple axiomatic proof
*)
Example PP: forall (P: formula), Provable(P --> P).
Proof.
intros.
assert (Provable (AxThen2 P P P)).
apply then2.
assert (Provable (AxThen1 P P)).
apply then1.

eapply MP in H0.
eapply MP.
eapply MP.

apply then2.
apply then1.
apply then1.

exact H.

Unshelve.
auto.
Qed.


(*
Syntactic validity of [](P /\' Q) --> ([]P /\' Q)
*)
Example AndK: forall (P Q:formula), 
    Provable ([](P /\' Q) --> ([]P /\' []Q)).
Proof.
intros.
apply DT; intro.

apply andI.

assert (Provable ([](P /\' Q --> P))).
apply Nec.
apply and1.

eapply Kmod.

eexact H0.
apply Hyp.
trivial.

assert (Provable ([](P /\' Q --> Q))).
apply Nec.
apply and2.
eapply Kmod.
eexact H0.
apply Hyp.
trivial.
Qed.


(*
Natural deduction (modal) of the problem of the red barns (graneros rojos in Spanish)
*)
Example GranerosRojosK: forall (Barn Red:formula),
    Provable((¬[]Barn) --> ([](Barn /\' Red)) --> ([]Barn)).
intros.
apply DT; intro.
apply DT; intro.

eapply Kmod.
apply Nec.
apply and1.
apply Hyp.
exact H0.
Qed.

(*Semantics*)
(*
Kripke frames, a set of possible worlds and a relation.
*)
Record frame:Type := { 
  W : Set;
  R : W -> W -> Prop}.
(*
Kripke models, a frame plus a labeling function
*)
Record kripke: Type := {
  F : frame;
  L : (W F) -> var -> Prop}.


(*
Satisfaction function. Note that our meta language is the usual operators of
reasoning in Coq.
*)

Fixpoint satS4 (M:kripke) (x:(W (F M))) (phi:formula): Prop :=
match phi with
| Fal => False
| (Atom v) => (L M x v)
| phi1 /\' phi2 => (satS4 M x phi1) /\ (satS4 M x phi2)
| phi1 \/' phi2 => (satS4 M x phi1) \/ (satS4 M x phi2)
| phi1 --> phi2 => (satS4 M x phi1) -> (satS4 M x phi2)
| [] phi => 
    forall y:(W (F M)), (R (F M) x y) -> (satS4 M y phi)
end.

Notation "M » w ||= phi" := (satS4 M w phi) (at level 30).

(* M |= phi *)
Definition M_satS4 (M:kripke) (phi:formula) := 
	forall w:(W (F M)), (satS4 M w phi).
Notation "M |= phi" := (M_satS4 M phi) (at level 30).

(* F |= phi *)
Definition valid_in_frame (F:frame) (phi:formula) :=
	forall L:(W F)->var->Prop, (M_satS4 (Build_kripke F L) phi).
Notation "F |= phi" := (valid_in_frame F phi) (at level 30).

(* |= phi *)
Definition valid (phi:formula) :=
	forall F:frame, valid_in_frame F phi.
Notation " ||= phi" := (valid phi) (at level 30).


(*
Some usual relation properties
*)
Hypothesis Rref: forall (M:kripke) (x:(W (F M))),
(R (F M) x x).

Hypothesis Rtra: forall (M:kripke) (x y z:(W (F M))),
(R (F M) x y) /\ (R (F M) y z) ->
(R (F M) x z).

Hypothesis Reuc: forall (M:kripke) (x y z:(W (F M))),
(R (F M) x y) /\ (R (F M) x z) ->
(R (F M) y z).

Hypothesis Rsim: forall (M:kripke) (x y:(W (F M))),
(R (F M) x y) ->
(R (F M) y x).



(*Ejemplos S4*)

Section dobleneg.

Hypotheses NN: forall (P:formula), (¬¬P) = (P).

Example Chellas820_1: Provable ((<> Ver) <--> (¬ [] Fal)).
Proof.
unfold Iif.
eapply andI.
unfold Diam.

apply DT; intro.
unfold Ver in H.
rewrite <- (NN Fal).
apply Hyp; trivial.

apply DT; intro.
unfold Diam.
unfold Ver.
rewrite <- (NN Fal) in H.
apply Hyp; trivial.
Qed.




Example Chellas820_2: Provable (([] Ver) <--> (¬ <> Fal)).
Proof.
unfold Iif.
apply andI.


apply DT; intro.
unfold Diam.
apply notI.
intro.
eapply notE.
apply Hyp; eexact H.
unfold Ver.
apply Hyp; trivial.


apply DT; intro.
unfold Diam in H.
apply Hyp in H.
rewrite (NN ([]¬Fal)) in H.
unfold Ver.
trivial.

Qed.

(*S5, pag. 331*)
Example HuthRyan521_3: forall (P:formula), 
Provable (([] <> [] P) --> ([] P)).
Proof.
intros.
apply DT; intros.
apply Hyp in H.
unfold Diam in H.

rewrite <- (NN ([]P)).
apply notI; intro.

eapply notE.
apply Tmod.
exact H.
rewrite (NN ([]¬[]P)).
apply Nec.
apply Hyp in H0.
trivial.
Qed.

End dobleneg.


(*K pag. 330*)
Example HuthRyan521_1: forall (P Q:formula), 
Provable (([] P /\' [] Q) --> [] (P /\' Q)).
Proof.
intros.
apply DT; intros.

eapply Kmod.
eapply Kmod.
apply Nec.
apply and3.

apply Hyp in H.
eapply andE1 in H.
trivial.

apply Hyp in H.
eapply andE2 in H.
trivial.
Qed.


(*S5, pag. 331*)
Example HuthRyan521_2: forall (P: formula), 
Provable (P --> ([] <> P)).
Proof.
intros.
apply DT; intros.
unfold Diam.

apply _5mod.
apply notI.

intros.

apply Hyp in H0.
apply Tmod in H0.

eapply notE.
apply Hyp in H.
eexact H.
trivial.
Qed.


(*Ejemplos semanticos*)

Lemma AxK_valid: forall (phi1 phi2:formula),
valid([] (phi1 --> phi2) --> [] phi1 --> [] phi2).
Proof.
intros.
unfold valid.
intros.
unfold valid_in_frame.
intros.
unfold M_satS4.
intros.
simpl.
intros.
apply H.
trivial.
apply H0.
trivial.
Qed.


Inductive W':Set :=
| w1:W'
| w2:W'
| w3:W'
| w4:W'.
Print W'.

Definition R' (x y:W') : Prop :=
match x with
| w1 => match y with
	| w1 => True
	| w3 => True
	| _ => False
	end
| w2 => False
| w3 => match y with
	| w2 => True
	| w4 => True
	| _ => False
	end
| w4 => False
end.

Definition F':=(Build_frame W' R').

Definition L' (x:W') (pv:var) :=
match x with
| w1 => match pv with
	| (p 0) => True
	| _ => False
	end
| w2 => False
| w3 => match pv with
	| (p 0) => True
	| (p 1) => True
	| _ => False
	end
| w4 => match pv with
	| (p 1) => True
	| _ => False
	end
end.

Definition K' := (Build_kripke F' L').



Example ejR: (R F' w1 w3).
simpl.
trivial.
Qed.

Example ejR': (not (R F' w1 w2)).
simpl.
unfold not.
intro.
trivial.
Qed.

Example ejL: (L K' w1 (p 0)).
simpl.
trivial.
Qed.